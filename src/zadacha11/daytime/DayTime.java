package zadacha11.daytime;

import java.util.Scanner;

/**
 * Программа считает количество часов, минут, секунд в n-ом количестве дней
 *
 * @author Nikita Lvov 18it18
 */
public class DayTime {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int days;
        do {
            System.out.println("Введите количество дней: ");
            days = scan.nextInt();
        } while (days <= 0);
        convertTime(days);
    }

    /**
     * Метод перевода дней в часы, минуты, секунды
     *
     * @param days количество дней
     */
    private static void convertTime(int days) {
        System.out.println("В " + days + " днях: " + days * 24 + " часов; " + days * 1440 + " минут; " + days * 86400 + " секунд.");
    }
}
