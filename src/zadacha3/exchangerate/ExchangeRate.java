package zadacha3.exchangerate;

import java.util.Scanner;

/**
 * Программа для перевода рублей в евро по заданному курсу
 *
 * @autor Lvov Nikita 18it18
 */
public class ExchangeRate {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите количество рублей, которые надо перевести: ");
        double money = sc.nextDouble();
        System.out.println("Введите курс: ");
        double course = sc.nextDouble();
        System.out.println(money + " рублей = " + convert(money, course) + " евро\n" +
                " По курсу: " + course + " рублей за 1 евро");
    }

    /**
     * Метод для перевода рублей в евро по заданному курсу
     *
     * @param money  рубли
     * @param course курс
     * @return соотношение рубля к евро
     */
    private static double convert(double money, double course) {
        return money / course;
    }
}
