package zadacha1.pointreading;

import java.util.Scanner;

/**
 * Программа считывающая количество символов, количество пробелов до точки при вводе строки
 *
 * @autor Lvov Nikita 18it18
 */
public class PointReading {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите строку: ");
        String s = scan.nextLine();
        int n = s.indexOf(".");// количество символов до точки
        System.out.println("Символов до точки: " + n);
        String s1 = s.split("\\.", 2)[0];// разбивает строку до первой точки и удаляет все что находится после
        System.out.print("Количество пробелов: ");
        System.out.println(s1.length() - s1.replaceAll(" ", "").length());// Убираем все пробелы из строки, а потом считаем разницу длин
    }
}
