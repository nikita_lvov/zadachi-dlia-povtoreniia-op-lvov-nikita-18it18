package zadacha5.primenumbers;

/**
 * Программа выводит простые числа от 2 до 100
 *
 * @autor Lvov Nikita 18it18
 */
public class PrimeNumbers {
    public static void main(String[] args) {
        //  Простые числа делятся без остатка только на 1 и на себя.
        //  Нужно посчитать делители, которых должно быть не больше 2.
        for (int i = 2; i <= 100; i++) {
            int v = 0;
            for (int j = 2; j < i; j++) {
                if ((i % j) == 0)
                    v++;
            }
            if (v < 1)
                System.out.println(i + " - простое число");
        }
    }
}
