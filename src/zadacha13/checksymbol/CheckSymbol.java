package zadacha13.checksymbol;

import java.util.Scanner;

/**
 * Класс проверяющий символ
 *
 * @author Nikita Lvov 18it18
 */
public class CheckSymbol {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите символ(ы): ");
        String string = sc.nextLine();
        char[] character = string.toCharArray();//создаем массив символов в строке

        for (char c : character) {
            if ((c >= 'A' && c <= 'Z') ||
                    (c >= 'a' && c <= 'z') ||
                    (c >= 'А' && c <= 'Я') ||
                    (c >= 'а' && c <= 'я')) {
                System.out.println(c + " - Это буква!");
            }
            if (c >= '0' && c <= '9') {
                System.out.println(c + " - Это цифра!");
            }
            if (c == ',' || c == '.' || c == '!' ||
                    c == '?' || c == ';' || c == ':') {
                System.out.println(c + " - Это знак пунктуации!");
            }
        }
    }
}

