package zadacha9.palindromenumber;

import java.util.Scanner;

/**
 * Класс для проверки массива целых чисел на палиндром
 *
 * @autor Nikita Lvov 18it18
 */
public class PalindromeNumber {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите количество чисел: ");
        int n = scan.nextInt();
        int[] numbers = new int[n];
        for (int i = 0; i < numbers.length; i++) {
            System.out.print("Введите " + (i + 1) + " число: ");
            numbers[i] = scan.nextInt();
        }
        System.out.println(isPalindrome(numbers) ? "Это Палиндром" : "Это не Палиндром");
    }

    /**
     * Метод сравнения чисел массива на палиндромность
     *
     * @param numbers массив чисел
     * @return сравнения чисел
     */
    private static boolean isPalindrome(int[] numbers) {
        boolean isPalindrome = true;
        for (int i = 0; i < (numbers.length + 1) / 2 && isPalindrome; i++) {
            isPalindrome = numbers[i] == numbers[numbers.length - i - 1];
        }
        return isPalindrome;
    }
}

