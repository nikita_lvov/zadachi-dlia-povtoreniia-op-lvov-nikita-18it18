package zadacha4.distancestrike;

/**
 * Расчет расстояния до места удара молнии.
 *
 * @autor Lvov Nikita 18it18
 */
public class DistanceStrike {
    public static void main(String[] args) {

        double distance;

        distance = 6.8 * 1234.8;

        System.out.println("Расстояние до места удара молнии = " + distance);

    }
}
