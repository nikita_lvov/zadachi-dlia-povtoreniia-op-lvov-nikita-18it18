package zadacha2.arrayIncrease;

import java.util.Scanner;

/**
 * Программа увеличивающая элемент массива на 10%
 *
 * @autor Lvov Nikita 18it18
 */
public class ArrayIncrease {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double[] mas = {1, 2, 3, 4, 5};
        System.out.println("Изначальный массив - " + java.util.Arrays.toString(mas));
        int number;
        do {
            System.out.println("Введите номер массива (от 0 до 4), который хотите увеличить на 10%: ");
            number = sc.nextInt();
        } while (number < 0 || number > 4);
        double[] mas2 = increaseMas(mas, number);// присваиваем массиву метод возвращающий массив с увеличеным элементом
        System.out.println("Конечный массив - " + java.util.Arrays.toString(mas2));// выводим полученный массив
    }

    /**
     * @param mas    Исходный массив
     * @param number Выбранный пользователем элемент массива
     * @return Конечный массив
     */
    private static double[] increaseMas(double[] mas, int number) {
        mas[number] = mas[number] * 1.1;
        return mas;
    }
}

