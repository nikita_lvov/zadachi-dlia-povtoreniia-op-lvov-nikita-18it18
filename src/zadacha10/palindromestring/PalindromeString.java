package zadacha10.palindromestring;

import java.util.Scanner;

/**
 * Класс проверки строки на палиндром:
 *
 * @author Nikita Lvov 18it18
 */

public class PalindromeString {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите строку: ");
        String s = scan.nextLine();
        if (isPalindrome(s)) {
            System.out.println("Это Палиндром!");
        } else {
            System.out.println("Это не Палиндром!");
        }
    }

    /**
     * Метод проверки строки на палиндромность
     *
     * @param text текст, который надо проверять
     * @return сравнение двух строк вне зависимости от регистра
     */
    private static boolean isPalindrome(String text) {
        text = text.replaceAll("([-\\s\"{},.;'`@#$%_^&*()№:!/?])", "");//удаляем все ненужное
        StringBuilder strBuilder = new StringBuilder(text);
        strBuilder.reverse(); //переворачиваем строку
        String invertedText = strBuilder.toString();//присваиваем перевернутую строку

        return text.equalsIgnoreCase(invertedText);//возвращаем сравнение двух строк вне зависимости от регистра
    }
}





