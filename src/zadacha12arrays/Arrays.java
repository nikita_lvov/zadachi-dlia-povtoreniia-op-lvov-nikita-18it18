package zadacha12arrays;

/**
 * Класс реализующий перенос чисел из двумерного массива в одомерный
 *
 * @author Nikita Lvov 18it18
 */
public class Arrays {
    public static void main(String[] args) {
        System.out.println("Двумерный массив: ");
        int[][] array2 = new int[5][5];
        array2Random(array2);
        System.out.println("Одномерный массив: ");
        System.out.println(java.util.Arrays.toString(array1Print(array2)));
    }

    /**
     * Метод рандомно заполняющий двумерный массив
     *
     * @param array2 двумерный массив
     */
    private static void array2Random(int[][] array2) {
        for (int i = 0; i < array2.length; i++) {
            int number = i + 1;
            System.out.print("Строка № " + number + " : ");
            for (int j = 0; j < array2[i].length; j++) {
                array2[i][j] = ((int) (Math.random() * 11) - 5);
                System.out.print(array2[i][j] + "\t");
            }
            System.out.println();
        }
    }

    /**
     * Метод реализующий перевод двумерного массива в одномерный
     *
     * @param array2 двумерный массив заполненный рандомными числами
     * @return одномерный массив
     */
    private static int[] array1Print(int[][] array2) {
        int[] array1 = new int[array2.length * array2.length];//Для хранения значений элементов строк,
        // создадим простой массив размером равным количеству строк двумерного массива
        for (int i = 0; i < array2.length; i++) {
            for (int j = 0; j < array2[i].length; j++) {
                array1[(i * array2[i].length) + j] = array2[i][j];
            }
        }
        return array1;
    }
}
