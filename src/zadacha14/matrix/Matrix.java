package zadacha14.matrix;

/**
 * Класс реализующий в квадратной матрице [4][4] замену столбцов и строк местами
 *
 * @author Nikita Lvov 18it18
 */
public class Matrix {
    public static void main(String[] args) {
        int n = 4;
        int[][] matrix = new int[n][n];
        initialMatrix(matrix);
        transposeMatrix(matrix);
    }

    /**
     * Метод заполняющий и выводящий на экран матрицу
     *
     * @param matrix матрица(двумерный массив) 4x4
     */
    private static void initialMatrix(int[][] matrix) {
        // заполняем матрицу числами
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                matrix[i][j] = 4 * i + j;
            }
        }
        System.out.println("Начальная матрица");
        System.out.println("-----------------");
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.printf("%4d", matrix[i][j]);
            }
            System.out.println();
        }
    }

    /**
     * Метод меняющий строки со столбцами в матрице и выводящий ее на экран
     *
     * @param matrix матрица(двумерный массив) 4x4
     */
    private static void transposeMatrix(int[][] matrix) {
        for (int i = 0; i < 4; i++) {
            for (int j = i + 1; j < 4; j++) {
                int tMatrix = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = tMatrix;
            }
        }
        System.out.println();
        System.out.println("Перевернутая матрица");
        System.out.println("--------------------");
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.printf("%4d", matrix[i][j]);
            }
            System.out.println();
        }
    }
}

