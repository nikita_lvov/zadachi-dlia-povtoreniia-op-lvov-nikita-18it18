package zadacha6.multiplicationtable;

import java.util.Scanner;

/**
 * Программа, которая выводит таблицу умножения введенного пользователем числа с клавиатуры.
 *
 * @autor Lvov Nikita 18it18
 */
public class MultiplicationTable {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        do {
            System.out.println("Введите число, таблицу умножения которого хотите увидеть: ");
            n = sc.nextInt();
        } while (n < 2 || n > 100);
        mulTable(n);
    }

    /**
     * Метод выполняющий умножение и вывод чисел
     *
     * @param n число которое надо перемножать
     */
    private static void mulTable(int n) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(n + " * " + i + " = " + n * i);
        }
    }
}
